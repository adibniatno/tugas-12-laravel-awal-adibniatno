<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
    @csrf
        <p>First Name:</p>
        <input type="text" name="pertama">
        <p>Last Name:</p>
        <input type="text" name="terakhir">

        <p>Gender</p>
        <input type="radio" name="gender" id="gender" value="male"> Male </br>
        <input type="radio" name="gender" id="gender" value="female"> Female </br>
        <input type="radio" name="gender" id="gender" value="other"> Other </br>

        <p>Nationality:</p>
        <select name="national">
            <option value="indonesia">Indonesian</option>
            <option value="singapura">Singapura</option>
            <option value="malaysia">Malaysia</option>
            <option value="thailand">Thailand</option>
        </select>
        
        <p>Language Spoken:</p>
        <input type="checkbox" name="bahasa" id="bahasa" value="indonesia"> Bahasa Indonesia </br>
        <input type="checkbox" name="bahasa" id="bahasa" value="english"> English </br>
        <input type="checkbox" name="bahasa" id="bahasa" value="arabic"> Arabic </br>
        <input type="checkbox" name="bahasa" id="bahasa" value="japanase"> Japanase </br>
        <input type="checkbox" name="bahasa" id="bahasa" value="other"> Other </br>

        <p>Bio:</p>
        <textarea name="bio" value="bio" cols="30" rows="10"></textarea> </br>

        <button type="submit">Sign Up</button>
    </form>
    <br>
</body>
</html>