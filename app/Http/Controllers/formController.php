<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class formController extends Controller
{
    public function form() 
    {
        return view("data.register");
    }

    public function post(Request $request)
    {

        // dd($request->all());
        $pertama = $request->pertama;
        $terakhir = $request->terakhir;
        $gender = $request->gender;
        $national = $request->national;
        $bahasa = $request->bahasa;
        $bio = $request->bio;

        return view('data.index', compact('pertama', 'terakhir', 'gender', 'national', 'bahasa', 'bio'));
    }
}
